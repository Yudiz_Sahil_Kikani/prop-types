import React, { Component } from 'react'
import PropTypes from 'prop-types'

class About extends Component {
  render () {
    return (
            <div>
            <h2>Name:- {this.props.name}</h2>
            <h2>Age:- {this.props.Age}</h2>
            <h2>Isboolean:- {this.props.isBoolean}</h2>
            <h2>skills:-{this.props.skills}</h2>
            </div>
    )
  }
};

About.propTypes={
    name: PropTypes.string.isRequired,
    Age: PropTypes.number,
    isBoolean: PropTypes.bool,
    skills: PropTypes.array,
}
export default About
